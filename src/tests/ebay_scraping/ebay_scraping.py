import numpy as np
import pandas as pd
import requests
from bs4 import BeautifulSoup
import os
import datetime

# TODO: Load in "Real Time" Items Pages
#       Can Use "selenium" or "splash" for this.

# TODO: Load "Main Category" name to main-categories.csv
#       Load "Main Category" name and "Sub Category" name to sub-categories.csv

# TODO: Load "Main Category" name and "Sub Category" name to items.csv

splashDomain = '10.0.0.6:8050'

rootUrl = 'https://www.ebay.com'

# Get csv file name
def getCSVName(name):
    return 'saves\\' + name + '.csv'


# Delete csv files
def deleteFiles(names):
    for name in names:
        if os.path.exists(getCSVName(name)):
            os.remove(getCSVName(name))

# Call HTTP Request
def httpRequest(url, useSplash = False):
    if useSplash:
        url = f'http://{splashDomain}/render.html?url={url}'

    res = requests.get(url)
    soup = BeautifulSoup(res.content, 'html.parser')

    return soup


# Load urls of the "main categories" pages to main_categories.csv
def loadMainCategories():
    main_categories_urls = []

    all_categories_url = f'{rootUrl}/n/all-categories'
    res = requests.get(all_categories_url)
    soup = BeautifulSoup(res.content, 'html.parser')

    categories = soup.select('.sub-cat-container .cat-title a')
    for category in categories:
        main_categories_urls.append(category.get('href'))

    df = pd.DataFrame({
        'URL': main_categories_urls,
        'Loaded': np.zeros(len(main_categories_urls), dtype=bool),
    })

    df.to_csv(getCSVName('main_categories'))

    print('loadMainCategories')


# Load urls of the "sub category" page to sub_categories.csv
def loadSubCategory(remove_anyway = False):
    main_categories_urls = pd.read_csv(getCSVName('main_categories'))
    main_categories_urls.drop(main_categories_urls.columns[[0]], axis=1, inplace=True)

    if main_categories_urls.shape[0] == 0:
        return False

    print('Urls to Load: ' + str(main_categories_urls.shape[0]))

    url = main_categories_urls.loc[0, 'URL']

    print('Load ' + url)
    loaded = False
    try:
        res = requests.get(url)
        soup = BeautifulSoup(res.content, 'html.parser')

        categories = soup.select('.b-accordion-subtree li a')

        sub_categories_urls = []

        for category in categories:
            sub_categories_urls.append(category.get('href'))

        if (os.path.exists(getCSVName('sub_categories'))):
            old_data = pd.read_csv(getCSVName('sub_categories'))
            old_data.drop(old_data.columns[[0]], axis=1, inplace=True)

            new_data = pd.DataFrame({
                'URL': sub_categories_urls,
                'Loaded': np.zeros(len(sub_categories_urls), dtype=bool),
            }, index=np.arange(old_data.shape[0], old_data.shape[0] + len(sub_categories_urls)))

            pd.concat([old_data, new_data]).to_csv(getCSVName('sub_categories'))
        else:
            new_data = pd.DataFrame({
                'URL': sub_categories_urls,
                'Loaded': np.zeros(len(sub_categories_urls), dtype=bool),
            })

            new_data.to_csv(getCSVName('sub_categories'))

        loaded = True
        print(url + ' Loaded')
    except:
        print('Can\'t load: ' + url)
    finally:
        if loaded or remove_anyway:
            main_categories_urls.loc[0, 'Loaded'] = True

            main_categories_urls = main_categories_urls.loc[main_categories_urls['Loaded'] == False]

            main_categories_urls.to_csv(getCSVName('main_categories'))
            print(url + ' Removed form waiting list')
            return True
        return False

# Load all the "sub categories" pages
def loadSubCategories():
    while (loadSubCategory()):
        print('')

# Load urls of items from single "items page" to items.csv
def loadItemsPage(remove_anyway = False):
    sub_categories_urls = pd.read_csv(getCSVName('sub_categories'))
    sub_categories_urls.drop(sub_categories_urls.columns[[0]], axis=1, inplace=True)

    if sub_categories_urls.shape[0] == 0:
        return False

    print('Urls to Load: ' + str(sub_categories_urls.shape[0]))

    url = sub_categories_urls.loc[0, 'URL']

    print('Load ' + url)
    loaded = False
    try:
        item_urls = []
        hasItems = True
        nextPageUrl = url
        while (hasItems):
            res = requests.get(nextPageUrl)
            soup = BeautifulSoup(res.content, 'html.parser')

            items = soup.select('.s-item__link')
            hasItems = len(items) != 0
            if hasItems:
                nextPage = soup.select_one('.pagination__next')

                for item in items:
                    item_urls.append(item.get('href'))

                nextPageUrl = nextPage.get('href')

                print("Next Page URL: " + nextPageUrl)
            else:
                print(nextPageUrl)
                print(items)
                print(soup.select_one('body'))

        if (os.path.exists(getCSVName('items'))):
            old_data = pd.read_csv(getCSVName('items'))
            old_data.drop(old_data.columns[[0]], axis=1, inplace=True)

            new_data = pd.DataFrame({
                'URL': item_urls,
                'Loaded': np.zeros(len(item_urls), dtype=bool),
                'Name': np.zeros(len(item_urls), dtype=str),
                'Price': np.zeros(len(item_urls), dtype=str),
                'Images Count': np.zeros(len(item_urls), dtype=int),
            }, index=np.arange(old_data.shape[0], old_data.shape[0] + len(item_urls)))

            pd.concat([old_data, new_data]).to_csv(getCSVName('items'))
        else:
            new_data = pd.DataFrame({
                'URL': item_urls,
                'Loaded': np.zeros(len(item_urls), dtype=bool),
                'Updated Date: ': np.zeros(len(item_urls), dtype=str),
                'Name': np.zeros(len(item_urls), dtype=str),
                'Price': np.zeros(len(item_urls), dtype=str),
                'Condition': np.zeros(len(item_urls), dtype=str),
                'Quantity Available': np.zeros(len(item_urls), dtype=str),
                'Quantity Sold': np.zeros(len(item_urls), dtype=str),
                'Images Count': np.zeros(len(item_urls), dtype=int),
            })

            new_data.to_csv(getCSVName('items'))

        loaded = True
        print(url + ' Loaded')
    except:
        print('Can\'t load: ' + url)
    finally:
        if loaded or remove_anyway:
            return False # TEMP
            sub_categories_urls.loc[0, 'Loaded'] = True

            sub_categories_urls = sub_categories_urls.loc[sub_categories_urls['Loaded'] == False]

            sub_categories_urls.to_csv(getCSVName('sub_categories'))
            print(url + ' Removed form waiting list')
            return True
        return False


# Load all the "items pages"
def loadItemPages():
    while (loadItemsPage()):
        print('')

# Load data of item from single "item" page to items.csv
def loadItem():
    items = pd.read_csv(getCSVName('items'))
    items.drop(items.columns[[0]], axis=1, inplace=True)

    unloadedIndexs = items.index[items['Loaded'] == False].tolist()

    print('Items to Load: ' + str(len(unloadedIndexs)))

    if len(unloadedIndexs) == 0:
        return False

    rowIndex = unloadedIndexs[0]

    url = items.loc[rowIndex, 'URL']

    print('Load ' + url)
    try:
        res = requests.get(url)
        soup = BeautifulSoup(res.content, 'html.parser')

        # Load Data

        print('Item Index: ' + str(rowIndex))

        item_title = soup.select_one('#itemTitle')
        item_title.select_one('span').decompose()
        item_title = item_title.getText()

        item_price = soup.select_one('#prcIsum')
        item_price = item_price.get('content')

        item_condition = soup.select_one('#vi-itm-cond')
        item_condition = item_condition.getText()

        item_quantity_qvailable = soup.select_one('#qtySubTxt span')
        item_quantity_qvailable = item_quantity_qvailable.getText().replace('\t', '').replace('\n', '')

        item_quantity_sold = soup.select_one('.vi-txt-underline')
        item_quantity_sold = item_quantity_sold.getText()

        item_images_count = len(soup.select('#vi_main_img_fs_slider #vi_main_img_fs li'))

        items.loc[rowIndex, 'Updated Date'] = datetime.datetime.now().strftime('%x')
        items.loc[rowIndex, 'Name'] = item_title
        items.loc[rowIndex, 'Price'] = item_price
        items.loc[rowIndex, 'Condition'] = item_condition
        items.loc[rowIndex, 'Quantity Available'] = item_quantity_qvailable
        items.loc[rowIndex, 'Quantity Sold'] = item_quantity_sold
        items.loc[rowIndex, 'Images Count'] = item_images_count
        items.loc[rowIndex, 'Loaded'] = True

        print(items.loc[rowIndex, :])

        items.to_csv(getCSVName('items'))

        print(url + ' Loaded')
        return True
    except:
        print('Can\'t load: ' + url)
        return False

# Load all the "item" pages
def loadItems():
    while (loadItem()):
        print('')


# Entry Point

#deleteFiles(['main_categories', 'sub_categories', 'items'])
#loadMainCategories()

# Load More Sub Categories
#loadSubCategories()

# Load Items Urls

deleteFiles(['items'])
loadItemsPage()

#loadItem()

print('END')
