import numpy as np
import pandas as pd

from scipy import stats as sst

import matplotlib.pyplot as plt
import seaborn as sns

x = np.arange(-10, 10, 1)

y0 = sst.norm.pdf(x)
y2 = y0 + 1

plt.plot(x, y0)
plt.scatter(x, y2)

plt.show()