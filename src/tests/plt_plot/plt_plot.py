import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

indexs = np.linspace(0, 10)

f1 = np.cos(indexs)
f2 = np.sin(indexs)

plt.plot(indexs, f1, "b")
plt.plot(indexs, f2, "r")

plt.show()
#plt.savefig("images/sin_and_cos_functions.png")