import json

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import cross_val_score

from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import RandomizedSearchCV

from sklearn import metrics

# read in the iris data
iris = load_iris()

# create X (features) and y (response)
X = iris.data
y = iris.target

# use train/test split with different random_state values
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=4)

# Create Modals
knn = KNeighborsClassifier(n_neighbors=5)
# n_neighbors = (k) from KNN
# algorithm =
#   ‘auto’: TODO
#   ‘ball_tree’: TODO
#   ‘kd_tree’: TODO
#   ‘brute’: TODO
#

#knn.fit(X_train, y_train)

#scores = cross_val_score(knn, X, y, cv=10, scoring='accuracy')

# cv = number of validations
# scoring = return info, now is 'accuracy'

# print(scores)

# TODO: learn TPU

param_grid = {
    'n_neighbors': list(range(1, 30 + 1)),
    'algorithm': ['auto', 'ball_tree', 'kd_tree', 'brute'],
    'weights': ['uniform', 'distance'],
    'leaf_size': np.arange(10, 60+1, 10).tolist()
}

def printPrteyObj(obj):
    print(json.dumps(obj, indent=2, separators=(',', ': ')))

printPrteyObj(param_grid)

def gridSearchCVExample():
    print('-------------------------')

    grid = GridSearchCV(knn, param_grid, cv=10, scoring='accuracy')
    grid.fit(X_train, y_train)

    score = grid.score(X_test, y_test)
    print(score)

    df = pd.DataFrame(grid.cv_results_)

    # print(df[['params', 'std_test_score']])

    printPrteyObj({
        'best_params': grid.best_params_,
        'best_score': grid.best_score_,
    })

def randomizedSearchCVExample(random_state):
    print('-------------------------')

    randSearch = RandomizedSearchCV(knn, param_grid, cv=10, scoring='accuracy', n_iter=10, random_state=random_state)
    randSearch.fit(X_train, y_train)

    score = randSearch.score(X_test, y_test)
    print(score)

    df = pd.DataFrame(randSearch.cv_results_)

    # print(df[['params', 'std_test_score']])

    printPrteyObj({
        'best_params': randSearch.best_params_,
        'best_score': randSearch.best_score_,
    })

randomizedSearchCVExample(10)

randomizedSearchCVExample(20)

gridSearchCVExample()