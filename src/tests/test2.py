import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import seaborn as sns

import sklearn
import sklearn.datasets
from sklearn import metrics

import sklearn.neighbors
from sklearn.linear_model import LogisticRegression

# load dataset
iris = sklearn.datasets.load_iris()

test_data = iris.data
test_targets = iris.target

def knnTest():

    # learning stage
    knn = sklearn.neighbors.KNeighborsClassifier(n_neighbors=1)

    # testing stage
    knn.fit(test_data, test_targets)

    new_data = [
        [5., 3.6, 1.4, 0.2],
        [5., 4.6, 2.4, 1.2],
        [5., 5.6, 3.4, 3.4],
        [5., 6.6, 4.4, 3.5]
    ]

    print(knn.predict(new_data))
    print(knn.score(test_data, test_targets))

def testLogreg():
    # learning stage
    logreg = LogisticRegression(solver='liblinear', max_iter=100)

    logreg.fit(test_data, test_targets)

    # testing stage
    new_data = [
        [5., 3.6, 1.4, 0.2],
        [5., 4.6, 2.4, 1.2],
        [5., 5.6, 3.4, 3.4],
        [5., 6.6, 4.4, 3.5]
    ]

    data_pred = logreg.predict(new_data)

    target_pred = logreg.predict(test_data)
    print(target_pred)
    print(logreg.score(test_data, test_targets))

    print(metrics.accuracy_score(test_targets, target_pred))
    print(metrics.confusion_matrix(test_targets, target_pred))

# entry point
testLogreg()

sns.pairplot(pd.DataFrame(test_data))

plt.show()

def f(a,b):
    logreg = LogisticRegression(solver='liblinear', max_iter=100)
    test_data = iris.data[:, a:b]
    logreg.fit(test_data, test_targets)
    print(logreg.score(test_data, test_targets))

# end
print("END")