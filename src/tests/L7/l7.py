from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics

# read in the iris data
iris = load_iris()

# create X (features) and y (response)
X = iris.data
y = iris.target

# use train/test split with different random_state values
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=4)

# check classification accuracy of KNN with K=5
knn = KNeighborsClassifier(n_neighbors=5)
knn.fit(X_train, y_train)
y_pred = knn.predict(X_test)
print(metrics.accuracy_score(y_test, y_pred))

# simulate splitting a dataset of 25 observations into 5 folds
from sklearn.model_selection import KFold
kf = KFold(n_splits=5, shuffle=False).split(range(25))

# print the contents of each training and testing set
print('{} {:^61} {}'.format('Iteration', 'Training set observations', 'Testing set observations'))
for iteration, data in enumerate(kf, start=1):
    print('{:^9} {} {:^25}'.format(iteration, data[0], str(data[1])))


# ----
from sklearn.model_selection import cross_val_score

# 10-fold cross-validation with K=5 for KNN (the n_neighbors parameter)
knn = KNeighborsClassifier(n_neighbors=5)
scores = cross_val_score(knn, X, y, cv=10, scoring='accuracy')
print(scores)

print(scores.mean())

import numpy as np
import matplotlib.pyplot as plt

def example(k_max, cv):
    k_range = list(range(1, k_max+1))
    k_scores = []
    for k in k_range:
        knn = KNeighborsClassifier(n_neighbors=k)
        scores = cross_val_score(knn, X, y, cv=cv, scoring='accuracy')
        k_scores.append(scores.mean())

        # print log
        #print('k= {:2} | Score {:4}'.format(k, scores.mean()))

    arr = np.array(k_scores)

    bestK = np.argmax(arr, axis=0) + 1

    knn = KNeighborsClassifier(n_neighbors=bestK)
    scores = cross_val_score(knn, X, y, cv=10, scoring='accuracy')

    # print log
    print('k= {:2} | Score {}'.format(bestK, scores.mean()))

    # plot the value of K for KNN (x-axis) versus the cross-validated accuracy (y-axis)
    plt.plot(k_range, k_scores)
    plt.xlabel('Value of K for KNN')
    plt.ylabel('Cross-Validated Accuracy')
    plt.show()

example(50, 10)

url = "https://towardsdatascience.com/linear-vs-polynomial-regression-walk-through-83ca4f2363a3"
print(url)

# Simple Linear - LogisticRegression
# Polynomial Regression - PolynomialRegression