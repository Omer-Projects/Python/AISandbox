import numpy as np
import pandas as pd

from scipy import stats as sst

import matplotlib.pyplot as plt
import seaborn as sns

x = np.arange(-10, 10, 1)

y0 = x*x
y1 = (x+1)*(x+1) + 1

sns.displot([x, y0, y1])

sns.displot([x, y0])
sns.displot([x, y1])

plt.show()