import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import seaborn as sns

x = np.linspace(0, 10)

plt.plot(x, x**2, 'b', label='x**2')
plt.plot(x, x**2 + 10*x, 'r.', label='x**3')
#plt.xlabel('X Axis Title')
#plt.ylabel('Y Axis Title')
plt.title('Figure/plot Title')

plt.show()