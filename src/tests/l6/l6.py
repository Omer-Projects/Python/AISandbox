import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import seaborn as sns

import sklearn
import sklearn.datasets
from sklearn import metrics

import sklearn.neighbors
from sklearn.linear_model import LogisticRegression

from sklearn.model_selection import train_test_split

# load dataset
data = pd.read_csv('data/Advertising.csv', index_col=0)

#
sns.pairplot(data, x_vars=['TV','Radio','Newspaper'], y_vars='Sales', height=7, aspect=0.7, kind='reg')
plt.show()

# create a Python list of feature names
feature_cols = ['TV', 'Radio', 'Newspaper']
X = data[feature_cols]

y = data['Sales']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)